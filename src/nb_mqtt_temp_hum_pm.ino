#include "AIS_BC95_API.h"
#include <SoftwareSerial.h>
#include <DFRobot_SHT20.h>
#include "Wire.h"

//=========== Call Class SHT20 ================
DFRobot_SHT20 sht20;

//=========== Call Class SofwareSerial ================
SoftwareSerial mySerial(10, 11); // RX, TX

//=========== Call Class AIS_BC(_API) ================
AIS_BC95_API nb;

//=========== Config Mqtt ================
String host = "mqtt.adcm.co.th";                          // Your IP address or mqtt server url
String port = "1883";                                     // Your server port
String clientID = "e2f0a5e9-332f-11ed-8305-0242ac120002"; // Your client id < 120 characters
String topic = "/adcm/smart-environment/data";            // Your topic     < 128 characters
String payload = "";                                      // Your payload   < 500 characters
String username = "adcm";                                 // username for mqtt server, username <= 100 characters
String password = "p@ssw0rd";                             // password for mqtt server, password <= 100 characters

unsigned int subQoS = 0;      // subQoS      : unsinged int : 0, 1, 2
unsigned int pubQoS = 0;      // pubQoS      : unsinged int : 0, 1, 2
unsigned int pubRetained = 0; // pubRetained : unsigned int : 0, 1
const long interval = 20000;  // time in millisecond
unsigned long previousMillis = 0;
int cnt = 0;

int temp;
int hum;
int pm2_5;
int pm10;

void setup()
{

  //=========== Setup Serial ================
  Serial.begin(9600);
  while (!Serial)
    ;
  mySerial.begin(9600);
  while (!mySerial)
    ;

  //=========== Setup AIS_BC95_API ================
  nb.debug = false;
  nb.begin();
  setupMQTT();
  nb.setCallback(callback);
  previousMillis = millis();

  //=========== Setup SHT20 ================
  sht20.initSHT20();
}
void loop()
{
  nb.MQTTresponse();
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= interval)
  {

    cnt++;
    connectStatus();
    nb.publish(topic, payload + String(cnt));
    //  nb.publish(topic, payload, pubQoS, pubRetained);
    previousMillis = currentMillis;
  }
}

//=========== MQTT Function ================
void setupMQTT()
{
  if (nb.connectMQTT(host, port, clientID, username, password))
  {
    nb.subscribe(topic, subQoS);
  }
  // nb.unsubscribe(topic);
}
void connectStatus()
{
  if (!nb.isMQTTConnected())
  {
    if (nb.debug)
      Serial.println("reconnectMQ ");
    setupMQTT();
  }
}
void callback(String &topic, String &payload)
{
  Serial.println("-------------------------------");
  Serial.println("# Message from Topic \"" + topic + "\" : " + payload);
}

//=========== PM Function ================
void pms()
{
  int index = 0;
  char value;
  char previousValue;

  while (mySerial.available())
  {
    value = mySerial.read();
    if ((index == 0 && value != 0x42) || (index == 1 && value != 0x4d))
    {
      Serial.println("Cannot find the data header.");
      break;
    }

    if (index == 4 || index == 6 || index == 8 || index == 10 || index == 12 || index == 14)
    {
      previousValue = value;
    }
    else if (index == 7)
    {
      pm2_5 = 256 * previousValue + value;
    }
    else if (index == 9)
    {
      pm10 = 256 * previousValue + value;
    }
    else if (index > 15)
    {
      break;
    }
    index++;
  }
  while (mySerial.available())
    mySerial.read();
}

//=========== Temp&Hum Function ================
void sht()
{
  hum = sht20.readHumidity();
  temp = sht20.readTemperature();
}
